﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject CardPrefab;
    public GameObject TierPrefab;
    public RectTransform center;

    public CardImages[] TiersAndCards;

    public GameObject TierParent;
    public Text PointsText;

    public float VertOffset = 4000f;
    public float HorOffset = 2000f;

    GameObject[] Tiers;
    GameObject[][] Cards;

    int CurrentTierShowing = 0;
    int[] CurrentCardShowing;
    int[] tierVertPosition;

    ScrollViewSnap viewsnap;

    [System.Serializable]
    public class CardImages
    {
        public Sprite[] CardImage;
        public int TierValue;
    }

    // Start is called before the first frame update
    void Start()
    {
        viewsnap = GetComponent<ScrollViewSnap>();

        Tiers = new GameObject[TiersAndCards.Length];
        Cards = new GameObject[TiersAndCards.Length][];

        CurrentCardShowing = new int[TiersAndCards.Length];
        tierVertPosition = new int[TiersAndCards.Length];


        for (int i = 0; i < TiersAndCards.Length; i++)
        {
            //Prepare Tiers
            Tiers[i] = Instantiate(TierPrefab, TierParent.transform);

            Tiers[i].GetComponent<RectTransform>().anchoredPosition =
                new Vector2(Tiers[i].GetComponent<RectTransform>().anchoredPosition.x + HorOffset * i,
                center.anchoredPosition.y);


            //Prepare Cards
            Cards[i] = new GameObject[TiersAndCards[i].CardImage.Length];

            for (int e = 0; e < TiersAndCards[i].CardImage.Length; e++)
            {
                //are we on the last card?
                if (e == TiersAndCards[i].CardImage.Length - 1)
                {
                    Cards[i][e] = Instantiate(CardPrefab, Tiers[i].transform);
                    Cards[i][e].GetComponent<RectTransform>().anchoredPosition =
                    new Vector2(0, VertOffset * -1);

                    Cards[i][e].GetComponent<Image>().sprite = TiersAndCards[i].CardImage[e];
                }
                else
                {
                    Cards[i][e] = Instantiate(CardPrefab, Tiers[i].transform);
                    Cards[i][e].GetComponent<RectTransform>().anchoredPosition =
                    new Vector2(0, VertOffset * e);

                    Cards[i][e].GetComponent<Image>().sprite = TiersAndCards[i].CardImage[e];
                }



            }
        }

        viewsnap.SetTeirs(Tiers);
        PointsText.text = "0/" + TiersAndCards[0].TierValue.ToString();
    }

    public void CardUp()
    {
        int tier = viewsnap.GetTier();

        tierVertPosition[tier]++;
        CurrentCardShowing[tier] = (int)Mathf.Repeat(tierVertPosition[tier], Cards[tier].Length);

        Cards[tier][(int)Mathf.Repeat(CurrentCardShowing[tier]-2, Cards[tier].Length)].GetComponent<RectTransform>().anchoredPosition =
    new Vector2(0, center.anchoredPosition.y + VertOffset * (tierVertPosition[tier]+Cards[tier].Length-2));

        StartCoroutine(LerpTeir(new Vector2(HorOffset * tier, center.anchoredPosition.y - VertOffset * tierVertPosition[tier]), tier));


        PointsText.text = "0/" + TiersAndCards[tier].TierValue.ToString();


    }

    public void cardDown()
    {
        int tier = viewsnap.GetTier();

        tierVertPosition[tier]--;
        CurrentCardShowing[tier] = (int)Mathf.Repeat(tierVertPosition[tier], Cards[tier].Length);



        Cards[tier][(int)Mathf.Repeat(CurrentCardShowing[tier] + Cards[tier].Length - 1, Cards[tier].Length)].GetComponent<RectTransform>().anchoredPosition =
new Vector2(0, center.anchoredPosition.y + VertOffset * (tierVertPosition[tier] -1));

        StartCoroutine(LerpTeir(new Vector2(HorOffset * tier, center.anchoredPosition.y - VertOffset * tierVertPosition[tier]), tier));


        PointsText.text = "0/" + TiersAndCards[tier].TierValue.ToString();


    }



    // Update is called once per frame
    void Update()
    {
        if(CurrentTierShowing != viewsnap.GetTier())
        {
            CurrentTierShowing = viewsnap.GetTier();
            PointsText.text = "0/" + TiersAndCards[CurrentTierShowing].TierValue.ToString();
        }


    }

    IEnumerator LerpTeir(Vector2 moveto, int tier)
    {
       
        float elapsedTime = 0;
        float waitTime = 1f;
        Vector2 startpos = Tiers[tier].GetComponent<RectTransform>().anchoredPosition;

        while (elapsedTime < waitTime)
        {
            Tiers[tier].GetComponent<RectTransform>().anchoredPosition  = Vector2.Lerp(startpos, moveto, (elapsedTime / waitTime));
            elapsedTime += Time.deltaTime *4;


            yield return null;
        }

        Tiers[tier].GetComponent<RectTransform>().anchoredPosition = moveto;

        yield return null;
    }
}
