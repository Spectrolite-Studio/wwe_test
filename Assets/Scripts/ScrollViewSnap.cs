﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewSnap : MonoBehaviour
{
    public bool verticle = false;
    public RectTransform panel;
    public RectTransform[] cards;
    public RectTransform center;

    private float[] distance;
    private bool dragging = false;
    private int cardDistance;
    private int minCardNum;

    bool HasCards = false;


    // Start is called before the first frame update


    public void SetTeirs(GameObject[] TheTiers)
    {
        cards = new RectTransform[TheTiers.Length];
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i] = TheTiers[i].GetComponent<RectTransform>();
        }

        int cardsLength = cards.Length;
        distance = new float[cardsLength];

        if (TheTiers.Length > 1)
            cardDistance = (int)Mathf.Abs(cards[1].anchoredPosition.x - cards[0].anchoredPosition.x);
        else
            cardDistance = 0;


        HasCards = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(HasCards)
        {
            for (int i = 0; i < cards.Length; i++)
            {

                 distance[i] = Mathf.Abs(center.transform.position.x - cards[i].transform.position.x);

            }

            float minDistance = Mathf.Min(distance);

            for (int i = 0; i < cards.Length; i++)
            {
                if (minDistance == distance[i])
                {
                    minCardNum = i;
                }
            }

            if (!dragging)
            {
                LerpButton(minCardNum * -cardDistance);

            }
        }
        
    }

    void LerpButton(int position)
    {

            float newX = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * 10f);
            //float newY = Mathf.Lerp(panel.anchoredPosition.y, center.position.y, Time.deltaTime * 10f);
            Vector2 newPos = new Vector2(newX, center.anchoredPosition.y);

            panel.anchoredPosition = newPos;

    }

    public  int GetTier()
    {
        return minCardNum;
    }


    public void StartDrag()
    {
        dragging = true;
    }

    public void EndDrag()
    {
        dragging = false;
    }
}
